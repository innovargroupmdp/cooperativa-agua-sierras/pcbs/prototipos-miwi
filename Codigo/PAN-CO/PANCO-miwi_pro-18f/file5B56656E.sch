EESchema Schematic File Version 2
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:analog_switches
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:bosch
LIBS:brooktre
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:conn
LIBS:contrib
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:display
LIBS:dsp
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic_symbols
LIBS:hc11
LIBS:infineon
LIBS:intel
LIBS:interface
LIBS:intersil
LIBS:ir
LIBS:Lattice
LIBS:leds
LIBS:LEM
LIBS:linear
LIBS:logic_programmable
LIBS:maxim
LIBS:mechanical
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:modules
LIBS:motor_drivers
LIBS:motorola
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:Oscillators
LIBS:philips
LIBS:power
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:regul
LIBS:relays
LIBS:rfcom
LIBS:RFSolutions
LIBS:sensors
LIBS:silabs
LIBS:siliconi
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:texas
LIBS:transf
LIBS:transistors
LIBS:triac_thyristor
LIBS:ttl_ieee
LIBS:valves
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:zetex
LIBS:Zilog
LIBS:Mcus
LIBS:PAN-CO-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6000 2700 2    60   Input ~ 0
VCCP
$Comp
L R R4
U 1 1 5B566E70
P 5800 2500
F 0 "R4" V 5900 2500 50  0000 C CNN
F 1 "R" V 5800 2500 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5730 2500 50  0001 C CNN
F 3 "" H 5800 2500 50  0001 C CNN
	1    5800 2500
	-1   0    0    1   
$EndComp
Text HLabel 5550 2350 0    60   Input ~ 0
VCCD
Text HLabel 4350 3900 0    60   Input ~ 0
VPP
Text HLabel 6850 3800 2    60   Input ~ 0
PGC
Text HLabel 6850 3900 2    60   Input ~ 0
PGD
Text HLabel 6050 5100 2    60   Input ~ 0
GNDD
$Comp
L R R3
U 1 1 5B566E71
P 4450 3600
F 0 "R3" V 4550 3600 50  0000 C CNN
F 1 "R" V 4450 3600 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4380 3600 50  0001 C CNN
F 3 "" H 4450 3600 50  0001 C CNN
	1    4450 3600
	-1   0    0    1   
$EndComp
Text HLabel 4350 3300 0    60   Input ~ 0
VCCD
Text HLabel 7650 3600 2    60   Output ~ 0
SCK
Text HLabel 6850 3700 2    60   Input ~ 0
SDI
Text HLabel 7950 4800 2    60   Output ~ 0
SDO
Text HLabel 6850 4500 2    60   Output ~ 0
CS
$Comp
L R R2
U 1 1 5B566E72
P 3150 4400
F 0 "R2" V 3250 4400 50  0000 C CNN
F 1 "R" V 3150 4400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3080 4400 50  0001 C CNN
F 3 "" H 3150 4400 50  0001 C CNN
	1    3150 4400
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5B566E73
P 2800 4400
F 0 "R1" V 2900 4400 50  0000 C CNN
F 1 "R" V 2800 4400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2730 4400 50  0001 C CNN
F 3 "" H 2800 4400 50  0001 C CNN
	1    2800 4400
	1    0    0    -1  
$EndComp
Text HLabel 2800 3900 2    60   Input ~ 0
VCCD
Text HLabel 7500 3300 2    60   Input ~ 0
SDA-i2c
Text HLabel 7500 3400 2    60   Input ~ 0
SDO-i2c
Text HLabel 3150 4650 2    60   Input ~ 0
SDA-i2c
Text HLabel 2800 4800 2    60   Input ~ 0
SDO-i2c
Text Notes 7950 3400 0    60   ~ 0
I2C-SOFT
Text HLabel 4950 3200 0    60   Input ~ 0
AN0
Text HLabel 4950 3300 0    60   Input ~ 0
AN1
Text HLabel 4950 3400 0    60   Input ~ 0
AN2
Text HLabel 4950 3500 0    60   Input ~ 0
AN3
Text HLabel 4950 3600 0    60   Input ~ 0
AN4
Text HLabel 4950 3700 0    60   Input ~ 0
AN5
Text HLabel 6850 3200 2    60   Input ~ 0
INT0
Text HLabel 6850 3400 2    60   Input ~ 0
INT3/2
Text HLabel 6850 3300 2    60   Input ~ 0
INT1
Text HLabel 6850 3500 2    60   Input ~ 0
AN9
Text HLabel 4950 4100 0    60   Input ~ 0
AN6
Text HLabel 4950 4200 0    60   Input ~ 0
AN7
Text HLabel 6850 4400 2    60   Input ~ 0
RO1
Text HLabel 6850 4300 2    60   Input ~ 0
DI1
Text Notes 2600 3750 0    60   ~ 0
I2C PULL-UPPS
Text Notes 3700 750  0    118  ~ 0
MICROCONTROLADOR Y LOGICA DE CONTROL DE BUS
Text HLabel 6850 4200 2    60   Input ~ 0
DE1
Text HLabel 6850 4100 2    60   Input ~ 0
!RE1
NoConn ~ 4950 3200
NoConn ~ 4950 3300
NoConn ~ 4950 3400
NoConn ~ 4950 3500
NoConn ~ 4950 3600
NoConn ~ 4950 3700
NoConn ~ 4950 4100
NoConn ~ 4950 4200
$Comp
L PWR_FLAG #FLG08
U 1 1 5B566E74
P 5950 2650
F 0 "#FLG08" H 5950 2725 50  0001 C CNN
F 1 "PWR_FLAG" H 5950 2800 50  0000 C CNN
F 2 "" H 5950 2650 50  0001 C CNN
F 3 "" H 5950 2650 50  0001 C CNN
	1    5950 2650
	1    0    0    -1  
$EndComp
$Comp
L C_Small C1
U 1 1 5B566E75
P 2700 2650
F 0 "C1" H 2710 2720 50  0000 L CNN
F 1 "C_Small" H 2710 2570 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 2700 2650 50  0001 C CNN
F 3 "" H 2700 2650 50  0001 C CNN
	1    2700 2650
	1    0    0    -1  
$EndComp
$Comp
L CP_Small C2
U 1 1 5B566E76
P 3100 2650
F 0 "C2" H 3110 2720 50  0000 L CNN
F 1 "CP_Small" H 3110 2570 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 3100 2650 50  0001 C CNN
F 3 "" H 3100 2650 50  0001 C CNN
	1    3100 2650
	1    0    0    -1  
$EndComp
Text HLabel 3100 2950 2    60   Input ~ 0
GNDD
Text HLabel 3100 2350 2    60   Input ~ 0
VCCP
$Comp
L PIC18F26J50_I/SP U1
U 1 1 5B575B69
P 5900 4000
F 0 "U1" H 5200 4950 50  0000 L CNN
F 1 "PIC18F26J50_I/SP" H 6600 4950 50  0000 R CNN
F 2 "Housings_DIP:DIP-28_Pitch2.54mm" H 5800 5100 100 0001 C CNN
F 3 "" H 5900 3950 50  0001 C CNN
	1    5900 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4400 6850 4400
Wire Wire Line
	6700 4300 6850 4300
Wire Wire Line
	5800 5100 6050 5100
Wire Wire Line
	4350 3900 5100 3900
Wire Wire Line
	4450 3750 4450 3900
Connection ~ 4450 3900
Wire Wire Line
	4350 3300 4450 3300
Wire Wire Line
	4450 3300 4450 3450
Wire Wire Line
	6700 3600 7650 3600
Wire Wire Line
	6850 4500 6700 4500
Wire Wire Line
	2800 3900 2800 4250
Wire Wire Line
	2800 4100 3150 4100
Wire Wire Line
	6700 3300 7500 3300
Wire Wire Line
	6700 3400 7500 3400
Wire Wire Line
	3150 4550 3150 4650
Wire Wire Line
	2800 4800 2800 4550
Wire Notes Line
	7450 3250 7450 3450
Wire Notes Line
	7450 3450 8400 3450
Wire Notes Line
	8400 3450 8400 3250
Wire Notes Line
	8400 3250 7450 3250
Wire Wire Line
	5100 3200 4950 3200
Wire Wire Line
	4950 3300 5100 3300
Wire Wire Line
	5100 3400 4950 3400
Wire Wire Line
	4950 3500 5100 3500
Wire Wire Line
	5100 3600 4950 3600
Wire Wire Line
	4950 3700 5100 3700
Wire Wire Line
	6850 3200 6700 3200
Wire Wire Line
	3150 4100 3150 4250
Connection ~ 2800 4100
Wire Notes Line
	2550 3800 2550 4900
Wire Notes Line
	2550 4900 3650 4900
Wire Notes Line
	3650 4900 3650 3800
Wire Notes Line
	3650 3800 2550 3800
Wire Wire Line
	6850 4100 6700 4100
Wire Wire Line
	6850 4200 6700 4200
Wire Wire Line
	4950 4100 5100 4100
Wire Wire Line
	4950 4200 5100 4200
Wire Wire Line
	5550 2350 5800 2350
Wire Wire Line
	5800 2650 5800 3000
Wire Wire Line
	5800 2700 6000 2700
Connection ~ 5800 2700
Wire Wire Line
	5950 2650 5950 2700
Connection ~ 5950 2700
Wire Wire Line
	2700 2550 3100 2550
Wire Wire Line
	2700 2750 3100 2750
Wire Wire Line
	3100 2550 3100 2350
Wire Wire Line
	3100 2750 3100 2950
Wire Wire Line
	6700 3900 6850 3900
Wire Wire Line
	5700 5000 5800 5000
Wire Wire Line
	5800 5000 5800 5100
Wire Wire Line
	6850 3500 6700 3500
NoConn ~ 6850 3500
Text HLabel 6850 4700 2    60   Input ~ 0
Wake
Text HLabel 6850 4600 2    60   Input ~ 0
!Reset
Wire Wire Line
	6850 4600 6700 4600
Wire Wire Line
	6850 4700 6700 4700
Wire Wire Line
	6850 3800 6700 3800
$Comp
L R R?
U 1 1 5B59058F
P 7550 3750
F 0 "R?" V 7650 3750 50  0000 C CNN
F 1 "R" V 7550 3750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7480 3750 50  0001 C CNN
F 3 "" H 7550 3750 50  0001 C CNN
	1    7550 3750
	-1   0    0    1   
$EndComp
$Comp
L R R?
U 1 1 5B5908DF
P 7550 4650
F 0 "R?" V 7650 4650 50  0000 C CNN
F 1 "R" V 7550 4650 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7480 4650 50  0001 C CNN
F 3 "" H 7550 4650 50  0001 C CNN
	1    7550 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4800 7950 4800
Wire Wire Line
	6850 3700 6700 3700
Connection ~ 7550 4800
Connection ~ 7550 3600
Text HLabel 7550 4000 2    60   Input ~ 0
VCCD
Text HLabel 7550 4400 2    60   Input ~ 0
VCCD
Wire Wire Line
	7550 4400 7550 4500
Wire Wire Line
	7550 3900 7550 4000
$EndSCHEMATC
