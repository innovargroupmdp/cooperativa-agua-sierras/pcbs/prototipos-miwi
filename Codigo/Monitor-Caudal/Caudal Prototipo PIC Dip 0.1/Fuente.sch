EESchema Schematic File Version 2
LIBS:Monitor-Caudal-rescue
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:microchip
LIBS:REGL
LIBS:Mcus
LIBS:battery
LIBS:Monitor-Caudal-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 8700 3500 2    60   Output ~ 0
3.3V
Text HLabel 8700 4150 2    60   Input ~ 0
0v
Text HLabel 3250 3650 0    60   Input ~ 0
Vccbat
Text HLabel 3250 4150 0    60   Output ~ 0
0Vbat
$Comp
L R R1
U 1 1 5B6559C4
P 6250 3650
F 0 "R1" V 6330 3650 50  0000 C CNN
F 1 "R" V 6250 3650 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6180 3650 50  0001 C CNN
F 3 "" H 6250 3650 50  0001 C CNN
	1    6250 3650
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5B655A58
P 6250 4000
F 0 "R2" V 6330 4000 50  0000 C CNN
F 1 "R" V 6250 4000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6180 4000 50  0001 C CNN
F 3 "" H 6250 4000 50  0001 C CNN
	1    6250 4000
	1    0    0    -1  
$EndComp
$Comp
L CP C2
U 1 1 5B655FA3
P 7000 3650
F 0 "C2" H 7025 3750 50  0000 L CNN
F 1 "CP" H 7025 3550 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 7038 3500 50  0001 C CNN
F 3 "" H 7000 3650 50  0001 C CNN
	1    7000 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 4150 8700 4150
Wire Wire Line
	3250 3650 4300 3650
Wire Wire Line
	4150 4050 4150 4150
Connection ~ 4150 4150
Wire Wire Line
	5300 3850 6250 3850
Wire Wire Line
	6250 3850 6250 3800
Connection ~ 4150 3650
Wire Wire Line
	5600 3500 8700 3500
Connection ~ 6250 4150
Wire Wire Line
	7000 3800 7000 4400
$Comp
L L L1
U 1 1 5B656F85
P 4450 3650
F 0 "L1" V 4400 3650 50  0000 C CNN
F 1 "L" V 4525 3650 50  0000 C CNN
F 2 "Inductors_SMD:L_1812_HandSoldering" H 4450 3650 50  0001 C CNN
F 3 "" H 4450 3650 50  0001 C CNN
	1    4450 3650
	0    -1   -1   0   
$EndComp
Connection ~ 6250 3500
Connection ~ 7000 4150
Connection ~ 7000 3500
$Comp
L +3.3VP #PWR08
U 1 1 5B66CEC0
P 5550 5900
F 0 "#PWR08" H 5700 5850 50  0001 C CNN
F 1 "+3.3VP" H 5550 6000 50  0000 C CNN
F 2 "" H 5550 5900 50  0001 C CNN
F 3 "" H 5550 5900 50  0001 C CNN
	1    5550 5900
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG09
U 1 1 5B66D033
P 5950 5800
F 0 "#FLG09" H 5950 5875 50  0001 C CNN
F 1 "PWR_FLAG" H 5950 5950 50  0000 C CNN
F 2 "" H 5950 5800 50  0001 C CNN
F 3 "" H 5950 5800 50  0001 C CNN
	1    5950 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 5900 5550 5900
Wire Wire Line
	5950 5800 5950 5900
$Comp
L +3.3VP #PWR010
U 1 1 5B66D145
P 7000 3250
F 0 "#PWR010" H 7150 3200 50  0001 C CNN
F 1 "+3.3VP" H 7000 3350 50  0000 C CNN
F 2 "" H 7000 3250 50  0001 C CNN
F 3 "" H 7000 3250 50  0001 C CNN
	1    7000 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3250 7000 3500
$Comp
L C C1
U 1 1 5B65FC15
P 4150 3900
F 0 "C1" H 4175 4000 50  0000 L CNN
F 1 "C" H 4175 3800 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 4188 3750 50  0001 C CNN
F 3 "" H 4150 3900 50  0001 C CNN
	1    4150 3900
	1    0    0    -1  
$EndComp
$Comp
L MCP1640-RESCUE-Monitor-Caudal U1
U 1 1 5B6B35D6
P 5000 3750
AR Path="/5B6B35D6" Ref="U1"  Part="1" 
AR Path="/5B5B4E91/5B6B35D6" Ref="U1"  Part="1" 
F 0 "U1" H 4850 3975 50  0000 C CNN
F 1 "MCP1640" H 5000 3975 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-6_Handsoldering" H 5000 4075 50  0001 C CNN
F 3 "" H 5000 3750 50  0001 C CNN
	1    5000 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4050 5000 4150
Connection ~ 5000 4150
Wire Wire Line
	4700 3650 4600 3650
Wire Wire Line
	4300 3650 4300 3750
Wire Wire Line
	4300 3750 4700 3750
Wire Wire Line
	5300 3650 5600 3650
Wire Wire Line
	5600 3650 5600 3500
Text HLabel 3250 4400 0    60   Input ~ 0
EN-swth
Wire Wire Line
	3250 4400 4400 4400
Wire Wire Line
	4400 4400 4400 3900
Wire Wire Line
	4400 3900 4700 3900
Wire Wire Line
	4150 3750 4150 3650
$Comp
L R R7
U 1 1 5B6C7F68
P 5650 4400
F 0 "R7" V 5730 4400 50  0000 C CNN
F 1 "R" V 5650 4400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5580 4400 50  0001 C CNN
F 3 "" H 5650 4400 50  0001 C CNN
	1    5650 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	5800 4400 5900 4400
Wire Wire Line
	5900 4400 5900 4150
Connection ~ 5900 4150
Wire Wire Line
	5500 4400 5400 4400
Wire Wire Line
	5400 4400 5400 4150
Connection ~ 5400 4150
$Comp
L R R9
U 1 1 5B6CEE58
P 6600 4400
F 0 "R9" V 6680 4400 50  0000 C CNN
F 1 "R" V 6600 4400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6530 4400 50  0001 C CNN
F 3 "" H 6600 4400 50  0001 C CNN
	1    6600 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	6450 4400 6250 4400
Wire Wire Line
	6250 4400 6250 4150
Wire Wire Line
	7000 4400 6750 4400
$EndSCHEMATC
