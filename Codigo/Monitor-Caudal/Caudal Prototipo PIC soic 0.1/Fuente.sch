EESchema Schematic File Version 4
LIBS:Monitor-Caudal-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 8700 3500 2    60   Output ~ 0
3.3V
Text HLabel 8700 4150 2    60   Input ~ 0
0v
Text HLabel 3250 3650 0    60   Input ~ 0
Vccbat
Text HLabel 3250 4150 0    60   Output ~ 0
0Vbat
$Comp
L Monitor-Caudal-rescue:R R8
U 1 1 5B6559C4
P 6250 3650
F 0 "R8" V 6330 3650 50  0000 C CNN
F 1 "R" V 6250 3650 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6180 3650 50  0001 C CNN
F 3 "" H 6250 3650 50  0001 C CNN
	1    6250 3650
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Caudal-rescue:R R9
U 1 1 5B655A58
P 6250 4000
F 0 "R9" V 6330 4000 50  0000 C CNN
F 1 "R" V 6250 4000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6180 4000 50  0001 C CNN
F 3 "" H 6250 4000 50  0001 C CNN
	1    6250 4000
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Caudal-rescue:CP C4
U 1 1 5B655FA3
P 7000 3650
F 0 "C4" H 7025 3750 50  0000 L CNN
F 1 "CP" H 7025 3550 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 7038 3500 50  0001 C CNN
F 3 "" H 7000 3650 50  0001 C CNN
	1    7000 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 4150 4150 4150
Wire Wire Line
	3250 3650 4150 3650
Wire Wire Line
	4150 4050 4150 4150
Connection ~ 4150 4150
Wire Wire Line
	5300 3850 6250 3850
Wire Wire Line
	6250 3850 6250 3800
Connection ~ 4150 3650
Wire Wire Line
	5600 3500 6250 3500
Connection ~ 6250 4150
Wire Wire Line
	7000 3800 7000 4150
$Comp
L Monitor-Caudal-rescue:L L1
U 1 1 5B656F85
P 4450 3650
F 0 "L1" V 4400 3650 50  0000 C CNN
F 1 "L" V 4525 3650 50  0000 C CNN
F 2 "Inductors_SMD:L_1812_HandSoldering" H 4450 3650 50  0001 C CNN
F 3 "" H 4450 3650 50  0001 C CNN
	1    4450 3650
	0    -1   -1   0   
$EndComp
Connection ~ 6250 3500
Connection ~ 7000 4150
Connection ~ 7000 3500
$Comp
L power:+3.3VP #PWR05
U 1 1 5B66CEC0
P 5550 5900
F 0 "#PWR05" H 5700 5850 50  0001 C CNN
F 1 "+3.3VP" H 5550 6000 50  0000 C CNN
F 2 "" H 5550 5900 50  0001 C CNN
F 3 "" H 5550 5900 50  0001 C CNN
	1    5550 5900
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG04
U 1 1 5B66D033
P 5950 5800
F 0 "#FLG04" H 5950 5875 50  0001 C CNN
F 1 "PWR_FLAG" H 5950 5950 50  0000 C CNN
F 2 "" H 5950 5800 50  0001 C CNN
F 3 "" H 5950 5800 50  0001 C CNN
	1    5950 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 5900 5550 5900
Wire Wire Line
	5950 5800 5950 5900
$Comp
L power:+3.3VP #PWR06
U 1 1 5B66D145
P 7000 3250
F 0 "#PWR06" H 7150 3200 50  0001 C CNN
F 1 "+3.3VP" H 7000 3350 50  0000 C CNN
F 2 "" H 7000 3250 50  0001 C CNN
F 3 "" H 7000 3250 50  0001 C CNN
	1    7000 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3250 7000 3500
$Comp
L Monitor-Caudal-rescue:C C3
U 1 1 5B65FC15
P 4150 3900
F 0 "C3" H 4175 4000 50  0000 L CNN
F 1 "C" H 4175 3800 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 4188 3750 50  0001 C CNN
F 3 "" H 4150 3900 50  0001 C CNN
	1    4150 3900
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Caudal-rescue:MCP1640-RESCUE-Monitor-Caudal U?
U 1 1 5B6B35D6
P 5000 3750
AR Path="/5B6B35D6" Ref="U?"  Part="1" 
AR Path="/5B5B4E91/5B6B35D6" Ref="U2"  Part="1" 
F 0 "U2" H 4850 3975 50  0000 C CNN
F 1 "MCP1640" H 5000 3975 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 5000 4075 50  0001 C CNN
F 3 "" H 5000 3750 50  0001 C CNN
	1    5000 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4050 5000 4150
Connection ~ 5000 4150
Wire Wire Line
	4700 3650 4600 3650
Wire Wire Line
	4300 3650 4300 3750
Wire Wire Line
	4300 3750 4700 3750
Wire Wire Line
	5300 3650 5600 3650
Wire Wire Line
	5600 3650 5600 3500
Text HLabel 3250 4400 0    60   Input ~ 0
EN-swth
Wire Wire Line
	3250 4400 4400 4400
Wire Wire Line
	4400 4400 4400 3900
Wire Wire Line
	4400 3900 4700 3900
Wire Wire Line
	4150 3750 4150 3650
$Comp
L Monitor-Caudal-rescue:R R7
U 1 1 5B6C7F68
P 5650 4400
F 0 "R7" V 5730 4400 50  0000 C CNN
F 1 "R" V 5650 4400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5580 4400 50  0001 C CNN
F 3 "" H 5650 4400 50  0001 C CNN
	1    5650 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	5800 4400 5900 4400
Wire Wire Line
	5900 4400 5900 4150
Connection ~ 5900 4150
Wire Wire Line
	5500 4400 5400 4400
Wire Wire Line
	5400 4400 5400 4150
Connection ~ 5400 4150
$Comp
L Monitor-Caudal-rescue:R R10
U 1 1 5B6CEE58
P 6600 4400
F 0 "R10" V 6680 4400 50  0000 C CNN
F 1 "R" V 6600 4400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6530 4400 50  0001 C CNN
F 3 "" H 6600 4400 50  0001 C CNN
	1    6600 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	6450 4400 6250 4400
Wire Wire Line
	6250 4400 6250 4150
Wire Wire Line
	7000 4400 6750 4400
Wire Wire Line
	4150 4150 5000 4150
Wire Wire Line
	4150 3650 4300 3650
Wire Wire Line
	6250 4150 7000 4150
Wire Wire Line
	6250 3500 7000 3500
Wire Wire Line
	7000 4150 7000 4400
Wire Wire Line
	7000 4150 8050 4150
Wire Wire Line
	7000 3500 7650 3500
Wire Wire Line
	5000 4150 5400 4150
Wire Wire Line
	5900 4150 6250 4150
Wire Wire Line
	5400 4150 5900 4150
$Comp
L Regulator_Linear:MCP1700-2502E_SOT23 U3
U 1 1 5D25E68C
P 8050 3800
F 0 "U3" H 8050 4042 50  0000 C CNN
F 1 "MCP1700-2502E_SOT23" H 8050 3951 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8050 4025 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001826D.pdf" H 8050 3800 50  0001 C CNN
	1    8050 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 4100 8050 4150
Connection ~ 8050 4150
Wire Wire Line
	8050 4150 8500 4150
Wire Wire Line
	7750 3800 7650 3800
Wire Wire Line
	7650 3800 7650 3500
Connection ~ 7650 3500
Wire Wire Line
	7650 3500 8700 3500
$Comp
L Device:C C5
U 1 1 5D268944
P 8500 3950
F 0 "C5" H 8615 3996 50  0000 L CNN
F 1 "C" H 8615 3905 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8538 3800 50  0001 C CNN
F 3 "~" H 8500 3950 50  0001 C CNN
	1    8500 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 3800 8500 3800
Wire Wire Line
	8500 4100 8500 4150
Connection ~ 8500 4150
Wire Wire Line
	8500 4150 8700 4150
Text HLabel 8700 3800 2    60   Output ~ 0
2.5V
Wire Wire Line
	8500 3800 8700 3800
Connection ~ 8500 3800
$EndSCHEMATC
