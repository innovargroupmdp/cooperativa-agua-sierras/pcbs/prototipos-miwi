EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:schema-gral-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4400 3100 500  400 
U 5B4E24F0
F0 "Fuente_swicht_485_1" 60
F1 "Fuente_swicht_485_1.sch" 60
F2 "L" I L 4400 3200 60 
F3 "N" I L 4400 3300 60 
F4 "VCC1" I R 4900 3300 60 
F5 "GND1" I R 4900 3200 60 
F6 "TIERRA" I L 4400 3400 60 
$EndSheet
$Sheet
S 4400 3950 1550 600 
U 5B4E24F2
F0 "Fuente_gral" 60
F1 "Fuente_gral.sch" 60
F2 "L" I L 4400 4050 60 
F3 "N" I L 4400 4150 60 
F4 "VCCD" O R 5950 4050 60 
F5 "GNDD" I R 5950 4150 60 
F6 "TIERRA" O L 4400 4250 60 
$EndSheet
$Comp
L Screw_Terminal_01x03 J1
U 1 1 5B53A886
P 3200 4150
F 0 "J1" H 3200 4350 50  0000 C CNN
F 1 "Screw_Terminal_01x03" H 3200 3950 50  0000 C CNN
F 2 "Connectors_Molex:Molex_KK-41791-03_03x3.96mm_Straight" H 3200 4150 50  0001 C CNN
F 3 "" H 3200 4150 50  0001 C CNN
	1    3200 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 4050 4400 4050
Wire Wire Line
	2000 4150 4400 4150
Wire Wire Line
	2250 4250 4400 4250
Connection ~ 3000 4050
Connection ~ 3000 4150
Connection ~ 3000 4250
Wire Wire Line
	4400 3200 4100 3200
Wire Wire Line
	4100 3200 4100 4050
Connection ~ 4100 4050
Wire Wire Line
	4400 3300 4000 3300
Wire Wire Line
	4000 3300 4000 4150
Connection ~ 4000 4150
Wire Wire Line
	4900 3300 8400 3300
Wire Wire Line
	4900 3200 8700 3200
$Comp
L AC #PWR01
U 1 1 5B4ED50B
P 2250 4000
F 0 "#PWR01" H 2250 3900 50  0001 C CNN
F 1 "AC" H 2250 4250 50  0000 C CNN
F 2 "" H 2250 4000 50  0001 C CNN
F 3 "" H 2250 4000 50  0001 C CNN
	1    2250 4000
	1    0    0    -1  
$EndComp
$Comp
L Earth #PWR02
U 1 1 5B4ED7D1
P 2000 4150
F 0 "#PWR02" H 2000 3900 50  0001 C CNN
F 1 "Earth" H 2000 4000 50  0001 C CNN
F 2 "" H 2000 4150 50  0001 C CNN
F 3 "" H 2000 4150 50  0001 C CNN
	1    2000 4150
	1    0    0    -1  
$EndComp
$Comp
L Earth_Protective #PWR03
U 1 1 5B4EDA97
P 2250 4350
F 0 "#PWR03" H 2500 4100 50  0001 C CNN
F 1 "Earth_Protective" H 2700 4200 50  0001 C CNN
F 2 "" H 2250 4250 50  0001 C CNN
F 3 "" H 2250 4250 50  0001 C CNN
	1    2250 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 4200 2250 4350
Wire Wire Line
	2250 4050 2250 4000
$Comp
L PWR_FLAG #FLG04
U 1 1 5B563723
P 2500 4000
F 0 "#FLG04" H 2500 4075 50  0001 C CNN
F 1 "PWR_FLAG" H 2500 4150 50  0000 C CNN
F 2 "" H 2500 4000 50  0001 C CNN
F 3 "" H 2500 4000 50  0001 C CNN
	1    2500 4000
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG05
U 1 1 5B5638F1
P 2000 4100
F 0 "#FLG05" H 2000 4175 50  0001 C CNN
F 1 "PWR_FLAG" H 2000 4250 50  0000 C CNN
F 2 "" H 2000 4100 50  0001 C CNN
F 3 "" H 2000 4100 50  0001 C CNN
	1    2000 4100
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG06
U 1 1 5B563AB8
P 2250 4200
F 0 "#FLG06" H 2250 4275 50  0001 C CNN
F 1 "PWR_FLAG" H 2250 4350 50  0000 C CNN
F 2 "" H 2250 4200 50  0001 C CNN
F 3 "" H 2250 4200 50  0001 C CNN
	1    2250 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 4000 2500 4050
Connection ~ 2500 4050
Wire Wire Line
	2000 4100 2000 4150
Connection ~ 2250 4250
Wire Wire Line
	4400 3400 4200 3400
Wire Wire Line
	4200 3400 4200 4250
Connection ~ 4200 4250
Wire Wire Line
	4200 3650 8100 3650
Connection ~ 4200 3650
$Comp
L Screw_Terminal_01x05 J2
U 1 1 5B522D59
P 9100 4150
F 0 "J2" H 9100 4350 50  0000 C CNN
F 1 "Screw_Terminal_01x05" H 9100 3950 50  0000 C CNN
F 2 "Connectors_Molex:Molex_KK-6410-05_05x2.54mm_Straight" H 9100 4150 50  0001 C CNN
F 3 "" H 9100 4150 50  0001 C CNN
	1    9100 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 3650 8100 4150
Wire Wire Line
	8100 4150 8900 4150
Wire Wire Line
	8700 3200 8700 4050
Wire Wire Line
	8700 4050 8900 4050
Wire Wire Line
	8400 3300 8400 3950
Wire Wire Line
	8400 3950 8900 3950
Wire Wire Line
	5950 4150 7750 4150
Wire Wire Line
	7750 4150 7750 4250
Wire Wire Line
	7750 4250 8900 4250
Wire Wire Line
	8900 4350 6300 4350
Wire Wire Line
	6300 4350 6300 4050
Wire Wire Line
	6300 4050 5950 4050
$EndSCHEMATC
