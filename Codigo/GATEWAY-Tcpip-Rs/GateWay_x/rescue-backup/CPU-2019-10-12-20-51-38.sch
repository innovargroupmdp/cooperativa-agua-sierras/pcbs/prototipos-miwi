EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:bosch
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic_symbols
LIBS:hc11
LIBS:infineon
LIBS:intersil
LIBS:ir
LIBS:Lattice
LIBS:leds
LIBS:LEM
LIBS:logic_programmable
LIBS:maxim
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic32mcu
LIBS:modules
LIBS:motor_drivers
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:rfcom
LIBS:RFSolutions
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:transf
LIBS:triac_thyristor
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:gateway-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PIC18F26K20_I/SO U?
U 1 1 5B52D92B
P 3800 4600
F 0 "U?" H 3100 5550 50  0000 L CNN
F 1 "PIC18F26K20_I/SO" H 4500 5550 50  0000 R CNN
F 2 "Housings_SOIC:SIOC-28_10.3x17.9mm_Pitch1.27mm" H 3700 5700 50  0001 C CNN
F 3 "" H 3800 4550 50  0001 C CNN
	1    3800 4600
	1    0    0    -1  
$EndComp
$Comp
L BATTERY BT?
U 1 1 5B52D933
P 1450 2550
F 0 "BT?" H 1450 2750 50  0000 C CNN
F 1 "BATTERY" H 1450 2360 50  0000 C CNN
F 2 "" H 1450 2550 60  0000 C CNN
F 3 "" H 1450 2550 60  0000 C CNN
	1    1450 2550
	0    1    1    0   
$EndComp
$Comp
L GND #PWR?
U 1 1 5B52D93C
P 900 2700
F 0 "#PWR?" H 900 2450 50  0001 C CNN
F 1 "GND" H 900 2550 50  0000 C CNN
F 2 "" H 900 2700 50  0001 C CNN
F 3 "" H 900 2700 50  0001 C CNN
	1    900  2700
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5B52D94B
P 2150 2300
F 0 "R?" V 2250 2300 50  0000 C CNN
F 1 "R" V 2150 2300 50  0000 C CNN
F 2 "" V 2080 2300 50  0001 C CNN
F 3 "" H 2150 2300 50  0001 C CNN
	1    2150 2300
	-1   0    0    1   
$EndComp
$Comp
L GNDD #PWR?
U 1 1 5B52D951
P 3600 5800
F 0 "#PWR?" H 3600 5550 50  0001 C CNN
F 1 "GNDD" H 3600 5675 50  0000 C CNN
F 2 "" H 3600 5800 50  0001 C CNN
F 3 "" H 3600 5800 50  0001 C CNN
	1    3600 5800
	1    0    0    -1  
$EndComp
$Comp
L 74LS155 U?
U 1 1 5B52D959
P 7400 5000
F 0 "U?" H 7350 5150 50  0000 C CNN
F 1 "74LS155" H 7400 4900 50  0000 C CNN
F 2 "" H 7400 5000 50  0001 C CNN
F 3 "" H 7400 5000 50  0001 C CNN
	1    7400 5000
	1    0    0    -1  
$EndComp
$Comp
L 74LS125 U?
U 1 1 5B52D95B
P 8650 2600
F 0 "U?" H 8650 2700 50  0000 L BNN
F 1 "74LS125" H 8700 2450 50  0000 L TNN
F 2 "" H 8650 2600 50  0001 C CNN
F 3 "" H 8650 2600 50  0001 C CNN
	1    8650 2600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8150 1900 8150 4600
Wire Wire Line
	8650 4700 8150 4700
Wire Wire Line
	8650 2900 8650 4700
Wire Wire Line
	8150 4800 9150 4800
Wire Wire Line
	8600 1600 9700 1600
Wire Wire Line
	7500 5450 7500 5450
Wire Wire Line
	8150 5100 8750 5100
Wire Wire Line
	8150 5200 8750 5200
Wire Wire Line
	8750 5300 8150 5300
$Comp
L +5V #PWR?
U 1 1 5B52D984
P 2100 2900
F 0 "#PWR?" H 2100 2750 50  0001 C CNN
F 1 "+5V" H 2100 3040 50  0000 C CNN
F 2 "" H 2100 2900 50  0001 C CNN
F 3 "" H 2100 2900 50  0001 C CNN
	1    2100 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 1600 7700 2600
Wire Wire Line
	7700 2600 7700 3850
Wire Wire Line
	7700 2600 8200 2600
Wire Wire Line
	6300 3850 7700 3850
Wire Wire Line
	7700 3850 8700 3850
Connection ~ 7700 2600
$Comp
L 74LS125 U?
U 1 1 5B52D988
P 9150 3850
F 0 "U?" H 9150 3950 50  0000 L BNN
F 1 "74LS125" H 9200 3700 50  0000 L TNN
F 2 "" H 9150 3850 50  0001 C CNN
F 3 "" H 9150 3850 50  0001 C CNN
	1    9150 3850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6300 3850 6300 5400
Wire Wire Line
	6300 5400 4600 5400
Connection ~ 7700 3850
Wire Wire Line
	4600 5300 6650 5300
$Comp
L GNDD #PWR?
U 1 1 5B52D989
P 6650 4750
F 0 "#PWR?" H 6650 4500 50  0001 C CNN
F 1 "GNDD" H 6650 4625 50  0000 C CNN
F 2 "" H 6650 4750 50  0001 C CNN
F 3 "" H 6650 4750 50  0001 C CNN
	1    6650 4750
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR?
U 1 1 5B52D98A
P 6650 5450
F 0 "#PWR?" H 6650 5200 50  0001 C CNN
F 1 "GNDD" H 6650 5325 50  0000 C CNN
F 2 "" H 6650 5450 50  0001 C CNN
F 3 "" H 6650 5450 50  0001 C CNN
	1    6650 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 4750 6650 4700
Wire Wire Line
	6650 5450 6650 5400
Wire Wire Line
	5600 4950 6650 4950
Wire Wire Line
	5400 5050 6650 5050
$Comp
L +5V #PWR?
U 1 1 5B52D98B
P 6650 4100
F 0 "#PWR?" H 6650 3950 50  0001 C CNN
F 1 "+5V" H 6650 4240 50  0000 C CNN
F 2 "" H 6650 4100 50  0001 C CNN
F 3 "" H 6650 4100 50  0001 C CNN
	1    6650 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 4100 6650 4600
Wire Wire Line
	9100 2600 9700 2600
Wire Wire Line
	3600 5600 3700 5600
Wire Wire Line
	3600 5600 3600 5700
Wire Wire Line
	3600 5700 3600 5800
Text HLabel 3850 3500 2    60   Input ~ 0
VCCP
$Comp
L R R?
U 1 1 5B4D6EEC
P 3700 3300
F 0 "R?" V 3800 3300 50  0000 C CNN
F 1 "R" V 3700 3300 50  0000 C CNN
F 2 "" V 3630 3300 50  0001 C CNN
F 3 "" H 3700 3300 50  0001 C CNN
	1    3700 3300
	-1   0    0    1   
$EndComp
$Comp
L +5V #PWR?
U 1 1 5B4D6FA2
P 3700 3000
F 0 "#PWR?" H 3700 2850 50  0001 C CNN
F 1 "+5V" H 3700 3140 50  0000 C CNN
F 2 "" H 3700 3000 50  0001 C CNN
F 3 "" H 3700 3000 50  0001 C CNN
	1    3700 3000
	1    0    0    -1  
$EndComp
Text HLabel 3450 3150 0    60   Input ~ 0
VCCD
Wire Wire Line
	3450 3150 3700 3150
Wire Wire Line
	3700 3150 3700 3000
Wire Wire Line
	3850 3500 3700 3500
Wire Wire Line
	3700 3450 3700 3500
Wire Wire Line
	3700 3500 3700 3600
Connection ~ 3700 3500
Connection ~ 3700 3150
Text HLabel 2150 4500 0    60   Input ~ 0
VPP
Text HLabel 4750 4400 2    60   Input ~ 0
PGC
Text HLabel 4750 4500 2    60   Input ~ 0
PGD
Text HLabel 3850 5700 2    60   Input ~ 0
GNDD
Wire Wire Line
	3600 5700 3850 5700
Connection ~ 3600 5700
Wire Wire Line
	2150 4500 2250 4500
Wire Wire Line
	2250 4500 3000 4500
$Comp
L R R?
U 1 1 5B4D822D
P 2250 4200
F 0 "R?" V 2350 4200 50  0000 C CNN
F 1 "R" V 2250 4200 50  0000 C CNN
F 2 "" V 2180 4200 50  0001 C CNN
F 3 "" H 2250 4200 50  0001 C CNN
	1    2250 4200
	-1   0    0    1   
$EndComp
Wire Wire Line
	2250 4350 2250 4500
Connection ~ 2250 4500
Text HLabel 2150 3900 0    60   Input ~ 0
VCCD
Wire Wire Line
	2150 3900 2250 3900
Wire Wire Line
	2250 3900 2250 4050
Wire Wire Line
	4600 4400 5600 4400
Wire Wire Line
	4600 4500 5400 4500
Text HLabel 4750 5000 2    60   Input ~ 0
SCK
Text HLabel 4750 5100 2    60   Input ~ 0
SDI
Text HLabel 4750 5200 2    60   Input ~ 0
SDO
Wire Wire Line
	4750 5000 4600 5000
Wire Wire Line
	4750 5100 4600 5100
Wire Wire Line
	4750 5200 4600 5200
Text HLabel 4750 4900 2    60   Input ~ 0
CS
Wire Wire Line
	4750 4900 4600 4900
$Comp
L R R?
U 1 1 5B4D9915
P 5400 4750
F 0 "R?" V 5500 4750 50  0000 C CNN
F 1 "R" V 5400 4750 50  0000 C CNN
F 2 "" V 5330 4750 50  0001 C CNN
F 3 "" H 5400 4750 50  0001 C CNN
	1    5400 4750
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5B4D9ABD
P 5600 4750
F 0 "R?" V 5700 4750 50  0000 C CNN
F 1 "R" V 5600 4750 50  0000 C CNN
F 2 "" V 5530 4750 50  0001 C CNN
F 3 "" H 5600 4750 50  0001 C CNN
	1    5600 4750
	-1   0    0    1   
$EndComp
$Comp
L R R?
U 1 1 5B4DA18D
P 1750 6700
F 0 "R?" V 1850 6700 50  0000 C CNN
F 1 "R" V 1750 6700 50  0000 C CNN
F 2 "" V 1680 6700 50  0001 C CNN
F 3 "" H 1750 6700 50  0001 C CNN
	1    1750 6700
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5B4DA1EB
P 1400 6700
F 0 "R?" V 1500 6700 50  0000 C CNN
F 1 "R" V 1400 6700 50  0000 C CNN
F 2 "" V 1330 6700 50  0001 C CNN
F 3 "" H 1400 6700 50  0001 C CNN
	1    1400 6700
	1    0    0    -1  
$EndComp
Text HLabel 1400 6200 2    60   Input ~ 0
VCCD
Wire Wire Line
	1400 6200 1400 6400
Wire Wire Line
	1400 6400 1400 6550
Wire Wire Line
	1400 6400 1750 6400
Text HLabel 6650 4200 2    60   Input ~ 0
VCCD
Text HLabel 4750 4200 2    60   Input ~ 0
SDA-i2c
Text HLabel 4750 4300 2    60   Input ~ 0
SDO-i2c
Wire Wire Line
	5400 4500 5400 4600
Wire Wire Line
	5400 4900 5400 5050
Wire Wire Line
	5600 4950 5600 4900
Wire Wire Line
	5600 4400 5600 4600
Wire Wire Line
	4750 4200 4600 4200
Wire Wire Line
	4750 4300 4600 4300
Text HLabel 1750 6950 2    60   Input ~ 0
SDA-i2c
Text HLabel 1400 7100 2    60   Input ~ 0
SDO-i2c
Wire Wire Line
	1750 6850 1750 6950
Wire Wire Line
	1400 7100 1400 6850
Text Notes 5200 4300 0    60   ~ 0
I2C-SOFT
Wire Notes Line
	4700 4150 4700 4350
Wire Notes Line
	4700 4350 5650 4350
Wire Notes Line
	5650 4350 5650 4150
Wire Notes Line
	5650 4150 4700 4150
Text HLabel 2850 3800 0    60   Input ~ 0
AN0
Text HLabel 2850 3900 0    60   Input ~ 0
AN1
Text HLabel 2850 4000 0    60   Input ~ 0
AN2
Text HLabel 2850 4100 0    60   Input ~ 0
AN3
Text HLabel 2850 4200 0    60   Input ~ 0
AN4
Text HLabel 2850 4300 0    60   Input ~ 0
AN5
Text HLabel 4750 3800 2    60   Input ~ 0
INT0
Text HLabel 4750 4000 2    60   Input ~ 0
INT3/2
Text HLabel 4750 3900 2    60   Input ~ 0
INT1
Text HLabel 4750 4100 2    60   Input ~ 0
AN9
Text HLabel 2850 4700 0    60   Input ~ 0
AN6
Text HLabel 2850 4800 0    60   Input ~ 0
AN7
Wire Wire Line
	3000 3800 2850 3800
Wire Wire Line
	2850 3900 3000 3900
Wire Wire Line
	3000 4000 2850 4000
Wire Wire Line
	2850 4100 3000 4100
Wire Wire Line
	3000 4200 2850 4200
Wire Wire Line
	2850 4300 3000 4300
Wire Wire Line
	4750 3800 4600 3800
Wire Wire Line
	4750 3900 4600 3900
Wire Wire Line
	4750 4000 4600 4000
$Comp
L 74LS125 U?
U 1 1 5B52D95A
P 8150 1600
F 0 "U?" H 8150 1700 50  0000 L BNN
F 1 "74LS125" H 8200 1450 50  0000 L TNN
F 2 "" H 8150 1600 50  0001 C CNN
F 3 "" H 8150 1600 50  0001 C CNN
	1    8150 1600
	-1   0    0    -1  
$EndComp
Text HLabel 9700 1600 2    60   Input ~ 0
RO1
Text HLabel 1050 3050 2    60   Input ~ 0
DEx
Text HLabel 9700 2600 2    60   Input ~ 0
RO2
Text HLabel 9700 3850 2    60   Input ~ 0
R1OUT
Wire Wire Line
	9700 3850 9600 3850
Text HLabel 8750 5300 2    60   Input ~ 0
T1IN
Text HLabel 8750 5200 2    60   Input ~ 0
DI2
Text HLabel 8750 5100 2    60   Input ~ 0
DI1
Wire Wire Line
	1750 6400 1750 6550
Connection ~ 1400 6400
Wire Wire Line
	9150 4800 9150 4150
Wire Notes Line
	1150 6100 1150 7200
Wire Notes Line
	1150 7200 2250 7200
Wire Notes Line
	2250 7200 2250 6100
Wire Notes Line
	2250 6100 1150 6100
Text Notes 1200 6050 0    60   ~ 0
I2C PULL-UPPS
Wire Notes Line
	5250 4450 5750 4450
Wire Notes Line
	5750 4450 5750 5150
Wire Notes Line
	5750 5150 5250 5150
Wire Notes Line
	5250 5150 5250 4450
Text Notes 5300 5250 0    60   ~ 0
RES. DE \nAIS. PRG.
Text HLabel 1050 3200 2    60   Input ~ 0
!REx
Text HLabel 1550 3000 2    60   Input ~ 0
CS3
Wire Wire Line
	4750 4700 4600 4700
Wire Wire Line
	4750 4800 4600 4800
$Comp
L 74164 U?
U 1 1 5B4DFF63
P 3700 1300
F 0 "U?" H 4050 1550 50  0000 C CNN
F 1 "74164" H 3950 400 50  0000 C CNN
F 2 "" H 3700 1300 60  0001 C CNN
F 3 "" H 3700 1300 60  0001 C CNN
	1    3700 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 1300 3150 1400
Text HLabel 3050 1050 0    60   Input ~ 0
VCCD
Wire Wire Line
	3150 1050 3050 1050
Text HLabel 4250 1350 2    60   Input ~ 0
CS0
Wire Wire Line
	4750 4100 4600 4100
Wire Wire Line
	3050 1300 3150 1300
Text HLabel 3050 1150 0    60   Input ~ 0
CS_clk
Text HLabel 3050 1300 0    60   Input ~ 0
CS_s
Wire Wire Line
	2400 4700 3000 4700
Wire Wire Line
	2400 4800 3000 4800
Text HLabel 2400 4700 0    60   Input ~ 0
CS_clk
Text HLabel 2400 4800 0    60   Input ~ 0
CS_s
Wire Wire Line
	3050 1150 3150 1150
Text HLabel 4250 1500 2    60   Input ~ 0
CS1
Text HLabel 4250 1600 2    60   Input ~ 0
CS2
Text HLabel 4250 1700 2    60   Input ~ 0
CS3
Text HLabel 4250 1800 2    60   Input ~ 0
CS4
Text HLabel 4250 1900 2    60   Input ~ 0
CS5
Text HLabel 4250 2000 2    60   Input ~ 0
CS6
Text HLabel 4250 2100 2    60   Input ~ 0
CS7
Text Notes 3700 750  0    118  ~ 0
MICROCONTROLADOR Y LOGICA DE CONTROL DE BUS
$EndSCHEMATC
