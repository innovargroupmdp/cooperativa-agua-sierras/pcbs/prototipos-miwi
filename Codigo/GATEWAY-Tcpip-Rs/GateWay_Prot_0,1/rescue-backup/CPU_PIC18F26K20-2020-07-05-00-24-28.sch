EESchema Schematic File Version 2
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:analog_switches
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:bosch
LIBS:brooktre
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:conn
LIBS:contrib
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:display
LIBS:dsp
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic_symbols
LIBS:hc11
LIBS:infineon
LIBS:intel
LIBS:interface
LIBS:intersil
LIBS:ir
LIBS:Lattice
LIBS:leds
LIBS:LEM
LIBS:linear
LIBS:logic_programmable
LIBS:maxim
LIBS:mechanical
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:modules
LIBS:motor_drivers
LIBS:motorola
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:Oscillators
LIBS:philips
LIBS:power
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:regul
LIBS:relays
LIBS:rfcom
LIBS:RFSolutions
LIBS:sensors
LIBS:silabs
LIBS:siliconi
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:texas
LIBS:transf
LIBS:transistors
LIBS:triac_thyristor
LIBS:ttl_ieee
LIBS:valves
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:zetex
LIBS:Zilog
LIBS:GateWay_Prot_0,1-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6000 2500 2    60   Input ~ 0
VCCP
Text HLabel 5550 2150 0    60   Input ~ 0
VCCD
Text HLabel 4250 3700 0    60   Input ~ 0
VPP
Text HLabel 6850 3600 2    60   Input ~ 0
PGC
Text HLabel 6850 3700 2    60   Input ~ 0
PGD
Text HLabel 5950 4900 2    60   Input ~ 0
GNDD
$Comp
L R R2
U 1 1 5B56796D
P 4350 3400
F 0 "R2" V 4450 3400 50  0000 C CNN
F 1 "R" V 4350 3400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4280 3400 50  0001 C CNN
F 3 "" H 4350 3400 50  0001 C CNN
	1    4350 3400
	-1   0    0    1   
$EndComp
Text HLabel 4250 3100 0    60   Input ~ 0
VCCD
Text HLabel 6850 4200 2    60   Output ~ 0
SCK
Text HLabel 9600 4550 2    60   Input ~ 0
SDI
Text HLabel 6850 4400 2    60   Output ~ 0
SDO
Text HLabel 6850 4100 2    60   Output ~ 0
CS
$Comp
L R R3
U 1 1 5B56796E
P 4650 5750
F 0 "R3" V 4750 5750 50  0000 C CNN
F 1 "R" V 4650 5750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4580 5750 50  0001 C CNN
F 3 "" H 4650 5750 50  0001 C CNN
	1    4650 5750
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5B56796F
P 4300 5750
F 0 "R1" V 4400 5750 50  0000 C CNN
F 1 "R" V 4300 5750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4230 5750 50  0001 C CNN
F 3 "" H 4300 5750 50  0001 C CNN
	1    4300 5750
	1    0    0    -1  
$EndComp
Text HLabel 4300 5250 2    60   Input ~ 0
VCCD
Text HLabel 6850 3400 2    60   Input ~ 0
SDA-i2c
Text HLabel 6850 3500 2    60   Input ~ 0
SDO-i2c
Text HLabel 4650 6000 2    60   Input ~ 0
SDA-i2c
Text HLabel 4300 6150 2    60   Input ~ 0
SDO-i2c
Text Notes 7300 3500 0    60   ~ 0
I2C-SOFT
Text HLabel 4950 3000 0    60   Input ~ 0
AN0
Text HLabel 4950 3100 0    60   Input ~ 0
AN1
Text HLabel 4950 3200 0    60   Input ~ 0
AN2
Text HLabel 4950 3300 0    60   Input ~ 0
AN3
Text HLabel 4950 3400 0    60   Input ~ 0
AN4
Text HLabel 4950 3500 0    60   Input ~ 0
AN5
Text HLabel 6850 3000 2    60   Input ~ 0
INT0
Text HLabel 6850 3200 2    60   Input ~ 0
INT3/2
Text HLabel 6850 3100 2    60   Input ~ 0
INT1
Text HLabel 6850 3300 2    60   Input ~ 0
AN9
Text HLabel 4950 3900 0    60   Input ~ 0
AN6
Text HLabel 4950 4000 0    60   Input ~ 0
AN7
Text HLabel 6850 4600 2    60   Input ~ 0
RO1
Text HLabel 6850 4500 2    60   Output ~ 0
DI1
Text Notes 4100 5100 0    60   ~ 0
I2C PULL-UPPS
Text Notes 3700 750  0    118  ~ 0
MICROCONTROLADOR Y LOGICA DE CONTROL DE BUS
Text HLabel 6850 4000 2    60   Output ~ 0
DE1
Text HLabel 6850 3900 2    60   Output ~ 0
!RE1
NoConn ~ 4950 3000
NoConn ~ 4950 3100
NoConn ~ 4950 3200
NoConn ~ 4950 3300
NoConn ~ 4950 3400
NoConn ~ 4950 3500
NoConn ~ 4950 3900
NoConn ~ 4950 4000
NoConn ~ 6700 3000
NoConn ~ 6700 3100
NoConn ~ 6700 3200
NoConn ~ 6700 3300
$Comp
L PWR_FLAG #FLG015
U 1 1 5B567970
P 5950 2450
F 0 "#FLG015" H 5950 2525 50  0001 C CNN
F 1 "PWR_FLAG" H 5950 2600 50  0000 C CNN
F 2 "" H 5950 2450 50  0001 C CNN
F 3 "" H 5950 2450 50  0001 C CNN
	1    5950 2450
	1    0    0    -1  
$EndComp
$Comp
L C_Small C1
U 1 1 5B567971
P 3950 2450
F 0 "C1" H 3960 2520 50  0000 L CNN
F 1 "C_Small" H 3960 2370 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 3950 2450 50  0001 C CNN
F 3 "" H 3950 2450 50  0001 C CNN
	1    3950 2450
	1    0    0    -1  
$EndComp
$Comp
L CP_Small C2
U 1 1 5B567972
P 4350 2450
F 0 "C2" H 4360 2520 50  0000 L CNN
F 1 "CP_Small" H 4360 2370 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 4350 2450 50  0001 C CNN
F 3 "" H 4350 2450 50  0001 C CNN
	1    4350 2450
	1    0    0    -1  
$EndComp
Text HLabel 4350 2750 2    60   Input ~ 0
GNDD
Text HLabel 4350 2150 2    60   Input ~ 0
VCCP
$Comp
L PIC18F26K20_I/SO U1
U 1 1 5B56A3A4
P 5900 3800
F 0 "U1" H 5200 4750 50  0000 L CNN
F 1 "PIC18F26K20_I/SO" H 6600 4750 50  0000 R CNN
F 2 "Housings_DIP:DIP-28_W7.62mm_Socket_LongPads" H 5800 4900 50  0001 C CNN
F 3 "" H 5900 3750 50  0001 C CNN
	1    5900 3800
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5B57FA1A
P 8000 4000
F 0 "R4" V 8100 4000 50  0000 C CNN
F 1 "R" V 8000 4000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7930 4000 50  0001 C CNN
F 3 "" H 8000 4000 50  0001 C CNN
	1    8000 4000
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 5B57FB8C
P 8200 4000
F 0 "R5" V 8300 4000 50  0000 C CNN
F 1 "R" V 8200 4000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8130 4000 50  0001 C CNN
F 3 "" H 8200 4000 50  0001 C CNN
	1    8200 4000
	1    0    0    -1  
$EndComp
Text HLabel 8000 3750 2    60   Input ~ 0
VCCD
$Comp
L R R7
U 1 1 5B599594
P 9200 4000
F 0 "R7" V 9300 4000 50  0000 C CNN
F 1 "R" V 9200 4000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 9130 4000 50  0001 C CNN
F 3 "" H 9200 4000 50  0001 C CNN
	1    9200 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	6700 4600 6850 4600
Wire Wire Line
	6700 4500 6850 4500
Wire Wire Line
	5700 4800 5800 4800
Wire Wire Line
	5700 4900 5950 4900
Wire Wire Line
	4250 3700 5100 3700
Wire Wire Line
	4350 3550 4350 3700
Connection ~ 4350 3700
Wire Wire Line
	4250 3100 4350 3100
Wire Wire Line
	4350 3100 4350 3250
Wire Wire Line
	6700 4200 8000 4200
Wire Wire Line
	6700 4400 8200 4400
Wire Wire Line
	6850 4100 6700 4100
Wire Wire Line
	4300 5250 4300 5600
Wire Wire Line
	4300 5450 4650 5450
Wire Wire Line
	6850 3400 6700 3400
Wire Wire Line
	6850 3500 6700 3500
Wire Wire Line
	4650 5900 4650 6000
Wire Wire Line
	4300 6150 4300 5900
Wire Notes Line
	6800 3350 6800 3550
Wire Notes Line
	6800 3550 7750 3550
Wire Notes Line
	7750 3550 7750 3350
Wire Notes Line
	7750 3350 6800 3350
Wire Wire Line
	5100 3000 4950 3000
Wire Wire Line
	4950 3100 5100 3100
Wire Wire Line
	5100 3200 4950 3200
Wire Wire Line
	4950 3300 5100 3300
Wire Wire Line
	5100 3400 4950 3400
Wire Wire Line
	4950 3500 5100 3500
Wire Wire Line
	6850 3000 6700 3000
Wire Wire Line
	6850 3100 6700 3100
Wire Wire Line
	6850 3200 6700 3200
Wire Wire Line
	4650 5450 4650 5600
Connection ~ 4300 5450
Wire Notes Line
	4050 5150 4050 6250
Wire Notes Line
	4050 6250 5150 6250
Wire Notes Line
	5150 6250 5150 5150
Wire Notes Line
	5150 5150 4050 5150
Wire Wire Line
	6850 3900 6700 3900
Wire Wire Line
	6850 4000 6700 4000
Wire Wire Line
	6850 3300 6700 3300
Wire Wire Line
	6700 3600 6850 3600
Wire Wire Line
	4950 3900 5100 3900
Wire Wire Line
	4950 4000 5100 4000
Wire Wire Line
	5700 4900 5700 4800
Wire Wire Line
	5550 2150 5800 2150
Wire Wire Line
	5800 2150 5800 2800
Wire Wire Line
	5800 2500 6000 2500
Connection ~ 5800 2500
Wire Wire Line
	5950 2450 5950 2500
Connection ~ 5950 2500
Wire Wire Line
	3950 2350 4350 2350
Wire Wire Line
	3950 2550 4350 2550
Wire Wire Line
	4350 2350 4350 2150
Wire Wire Line
	4350 2550 4350 2750
Wire Wire Line
	6700 3700 6850 3700
Wire Wire Line
	8000 4200 8000 4150
Wire Wire Line
	8200 4400 8200 4150
Wire Wire Line
	8000 3850 8200 3850
Wire Wire Line
	8000 3750 8000 3850
Wire Wire Line
	9050 4550 9250 4550
Wire Wire Line
	6700 4300 8750 4300
Wire Wire Line
	9200 4150 9200 4550
Connection ~ 9200 4550
Text HLabel 9200 3750 2    60   Input ~ 0
VCCD
Wire Wire Line
	9200 3750 9200 3850
$Comp
L BC558 Q1
U 1 1 5B599A92
P 8850 4550
F 0 "Q1" H 9050 4625 50  0000 L CNN
F 1 "BC558" H 9050 4550 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Molded_Wide_Oval" H 9050 4475 50  0001 L CIN
F 3 "" H 8850 4550 50  0001 L CNN
	1    8850 4550
	-1   0    0    1   
$EndComp
Text HLabel 8750 5050 2    60   Input ~ 0
GNDD
$Comp
L R R8
U 1 1 5B59B5C9
P 9400 4550
F 0 "R8" V 9500 4550 50  0000 C CNN
F 1 "R" V 9400 4550 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 9330 4550 50  0001 C CNN
F 3 "" H 9400 4550 50  0001 C CNN
	1    9400 4550
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 5B59B680
P 8750 4000
F 0 "R6" V 8850 4000 50  0000 C CNN
F 1 "R" V 8750 4000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8680 4000 50  0001 C CNN
F 3 "" H 8750 4000 50  0001 C CNN
	1    8750 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 4150 8750 4350
Text HLabel 8750 3750 2    60   Input ~ 0
VCCD
Wire Wire Line
	8750 3750 8750 3850
Wire Wire Line
	8750 4750 8750 5050
Wire Wire Line
	9600 4550 9550 4550
Connection ~ 8750 4300
$EndSCHEMATC
