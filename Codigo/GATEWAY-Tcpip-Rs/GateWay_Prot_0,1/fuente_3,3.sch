EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GateWay_Prot_0,1-rescue:AZ1117-3.3 U2
U 1 1 5B59313F
P 5550 3450
F 0 "U2" H 5400 3575 50  0000 C CNN
F 1 "AZ1117-3.3" H 5550 3575 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-223-3Lead_TabPin2" H 5550 3700 50  0001 C CIN
F 3 "" H 5550 3450 50  0001 C CNN
	1    5550 3450
	1    0    0    -1  
$EndComp
$Comp
L GateWay_Prot_0,1-rescue:CP C3
U 1 1 5B5932DE
P 4800 3600
F 0 "C3" H 4825 3700 50  0000 L CNN
F 1 "CP" H 4825 3500 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 4838 3450 50  0001 C CNN
F 3 "" H 4800 3600 50  0001 C CNN
	1    4800 3600
	1    0    0    -1  
$EndComp
$Comp
L GateWay_Prot_0,1-rescue:CP C4
U 1 1 5B593329
P 6300 3600
F 0 "C4" H 6325 3700 50  0000 L CNN
F 1 "CP" H 6325 3500 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 6338 3450 50  0001 C CNN
F 3 "" H 6300 3600 50  0001 C CNN
	1    6300 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3750 4800 3900
Wire Wire Line
	4350 3900 6300 3900
Wire Wire Line
	5550 3900 5550 3750
Wire Wire Line
	6300 3900 6300 3750
Connection ~ 5550 3900
Wire Wire Line
	5850 3450 6750 3450
Wire Wire Line
	4350 3450 5250 3450
Text HLabel 4350 3450 0    60   Input ~ 0
5v
Text HLabel 6750 3450 0    60   Input ~ 0
3.3v
Text HLabel 4350 3900 0    60   Input ~ 0
gnd
Connection ~ 4800 3450
Connection ~ 4800 3900
Connection ~ 6300 3450
$EndSCHEMATC
