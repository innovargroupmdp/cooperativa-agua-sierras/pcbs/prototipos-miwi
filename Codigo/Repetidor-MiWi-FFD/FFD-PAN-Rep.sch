EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:microchip
LIBS:REGL
LIBS:FFD-PAN-Rep-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7350 2200 0    60   ~ 0
ICSP\n
Text Notes 7450 3750 0    60   ~ 0
I2C
Text Notes 7400 3200 0    60   ~ 0
SPI\n
$Comp
L PWR_FLAG #FLG01
U 1 1 5B5B4E8C
P 2050 3200
F 0 "#FLG01" H 2050 3275 50  0001 C CNN
F 1 "PWR_FLAG" H 2050 3350 50  0000 C CNN
F 2 "" H 2050 3200 50  0001 C CNN
F 3 "" H 2050 3200 50  0001 C CNN
	1    2050 3200
	1    0    0    -1  
$EndComp
$Comp
L +5C #PWR02
U 1 1 5B5B4E8D
P 1500 3250
F 0 "#PWR02" H 1500 3100 50  0001 C CNN
F 1 "+5C" H 1500 3390 50  0000 C CNN
F 2 "" H 1500 3250 50  0001 C CNN
F 3 "" H 1500 3250 50  0001 C CNN
	1    1500 3250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5B5B4E8E
P 2050 3350
F 0 "#PWR03" H 2050 3100 50  0001 C CNN
F 1 "GND" H 2050 3200 50  0000 C CNN
F 2 "" H 2050 3350 50  0001 C CNN
F 3 "" H 2050 3350 50  0001 C CNN
	1    2050 3350
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG04
U 1 1 5B5B4E8F
P 1300 3250
F 0 "#FLG04" H 1300 3325 50  0001 C CNN
F 1 "PWR_FLAG" H 1300 3400 50  0000 C CNN
F 2 "" H 1300 3250 50  0001 C CNN
F 3 "" H 1300 3250 50  0001 C CNN
	1    1300 3250
	1    0    0    -1  
$EndComp
$Comp
L +5C #PWR05
U 1 1 5B5B4E90
P 2200 4600
F 0 "#PWR05" H 2200 4450 50  0001 C CNN
F 1 "+5C" H 2200 4740 50  0000 C CNN
F 2 "" H 2200 4600 50  0001 C CNN
F 3 "" H 2200 4600 50  0001 C CNN
	1    2200 4600
	1    0    0    -1  
$EndComp
Wire Notes Line
	6850 2500 7300 2500
Wire Notes Line
	7300 1850 7300 2950
Wire Notes Line
	7300 1850 6850 1850
Wire Notes Line
	6850 1850 6850 2950
Wire Notes Line
	6850 3650 6850 3900
Wire Notes Line
	6850 3900 7350 3900
Wire Notes Line
	7350 3650 6850 3650
Wire Notes Line
	6850 3000 6850 3500
Wire Notes Line
	6850 3500 7350 3500
Wire Notes Line
	7350 3500 7350 3000
Wire Notes Line
	7350 3000 6850 3000
Wire Notes Line
	6850 2950 7300 2950
Wire Notes Line
	7350 3900 7350 3650
Wire Wire Line
	6300 2000 6950 2000
Wire Wire Line
	6300 2100 6950 2100
Wire Wire Line
	6300 2200 6950 2200
Wire Wire Line
	6300 2300 6950 2300
Wire Wire Line
	6300 3100 6950 3100
Wire Wire Line
	6950 3400 6300 3400
Wire Wire Line
	8250 2000 8400 2000
Wire Wire Line
	8400 2000 8400 1650
Wire Wire Line
	1500 3250 1500 3350
Wire Wire Line
	1500 3350 1300 3350
Wire Wire Line
	1300 3350 1300 3250
Wire Wire Line
	2050 3200 2050 3350
Wire Wire Line
	8400 1650 5000 1650
Wire Wire Line
	4900 1700 8350 1700
Wire Wire Line
	8350 1700 8350 2100
Wire Wire Line
	8350 2100 8250 2100
Wire Wire Line
	6300 4150 6950 4150
Wire Wire Line
	6950 3200 6300 3200
Wire Wire Line
	6950 3300 6300 3300
Wire Wire Line
	6950 4350 6300 4350
Wire Wire Line
	6950 4450 6300 4450
$Comp
L Earth_Protective #PWR06
U 1 1 5B5B4E92
P 2550 3250
F 0 "#PWR06" H 2800 3000 50  0001 C CNN
F 1 "Earth_Protective" H 3000 3100 50  0001 C CNN
F 2 "" H 2550 3150 50  0001 C CNN
F 3 "" H 2550 3150 50  0001 C CNN
	1    2550 3250
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG07
U 1 1 5B5B4E93
P 2550 3200
F 0 "#FLG07" H 2550 3275 50  0001 C CNN
F 1 "PWR_FLAG" H 2550 3350 50  0000 C CNN
F 2 "" H 2550 3200 50  0001 C CNN
F 3 "" H 2550 3200 50  0001 C CNN
	1    2550 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 3200 2550 3250
Wire Wire Line
	1950 4600 3550 4600
$Comp
L GND #PWR08
U 1 1 5B5B4E95
P 2200 4750
F 0 "#PWR08" H 2200 4500 50  0001 C CNN
F 1 "GND" H 2200 4600 50  0000 C CNN
F 2 "" H 2200 4750 50  0001 C CNN
F 3 "" H 2200 4750 50  0001 C CNN
	1    2200 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 4750 2200 4700
Wire Wire Line
	1950 4700 3550 4700
Wire Wire Line
	5000 4600 4550 4600
Wire Wire Line
	5000 1650 5000 4600
Wire Wire Line
	4900 4700 4550 4700
Wire Wire Line
	4900 1700 4900 4700
Wire Wire Line
	4900 2100 5450 2100
Connection ~ 4900 2100
Wire Wire Line
	5000 2000 5450 2000
Connection ~ 5000 2000
$Sheet
S 5450 1950 850  2800
U 5B5B4E89
F0 "Puertos_IO" 60
F1 "file5B5B4E89.sch" 60
F2 "SDI" I R 6300 3300 60 
F3 "CS" I R 6300 3400 60 
F4 "VCCD" I L 5450 2000 60 
F5 "INT" O R 6300 4150 60 
F6 "SDO" O R 6300 3200 60 
F7 "SCK" I R 6300 3100 60 
F8 "Reset" I R 6300 4450 60 
F9 "GNDD" I L 5450 2100 60 
F10 "VPP" O R 6300 2000 60 
F11 "VCCP" O R 6300 2100 60 
F12 "PGD" B R 6300 2200 60 
F13 "PGC" O R 6300 2300 60 
F14 "Wake" I R 6300 4350 60 
$EndSheet
$Sheet
S 3550 4450 1000 800 
U 5B5B4E91
F0 "Fuente" 60
F1 "file5B5B4E91.sch" 60
F2 "3.3V" O R 4550 4600 60 
F3 "0v" I R 4550 4700 60 
F4 "6Vbat" I L 3550 4600 60 
F5 "0Vbat" O L 3550 4700 60 
$EndSheet
$Sheet
S 6950 1950 1300 2800
U 5B5B4E88
F0 "CPU PIC18F26K20" 60
F1 "file5B5B4E88.sch" 60
F2 "VCCP" I L 6950 2100 60 
F3 "VCCD" I R 8250 2000 60 
F4 "VPP" I L 6950 2000 60 
F5 "PGC" I L 6950 2300 60 
F6 "PGD" I L 6950 2200 60 
F7 "GNDD" O R 8250 2100 60 
F8 "SCK" O L 6950 3100 60 
F9 "SDI" I L 6950 3200 60 
F10 "SDO" O L 6950 3300 60 
F11 "CS" O L 6950 3400 60 
F12 "SDA-i2c" I L 6950 3700 60 
F13 "SDO-i2c" I L 6950 3800 60 
F14 "AN0" I R 8250 3350 60 
F15 "AN1" I R 8250 3450 60 
F16 "AN2" I R 8250 3550 60 
F17 "AN3" I R 8250 3700 60 
F18 "AN4" I R 8250 3800 60 
F19 "AN5" I R 8250 3900 60 
F20 "INT0" I L 6950 4150 60 
F21 "INT3/2" I L 6950 4050 60 
F22 "INT1" I L 6950 4250 60 
F23 "AN9" I R 8250 4250 60 
F24 "AN6" I R 8250 4350 60 
F25 "AN7" I R 8250 4450 60 
F26 "Wake" O L 6950 4350 60 
F27 "!Reset" O L 6950 4450 60 
F28 "Vbat" I L 6950 2650 60 
$EndSheet
Connection ~ 2200 4600
Connection ~ 2200 4700
$Comp
L Conn_01x04_Male J4
U 1 1 5B662D24
P 1750 4700
F 0 "J4" H 1750 4900 50  0000 C CNN
F 1 "Conn_01x04_Male" H 1750 4400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 1750 4700 50  0001 C CNN
F 3 "" H 1750 4700 50  0001 C CNN
	1    1750 4700
	1    0    0    -1  
$EndComp
$Comp
L Earth_Protective #PWR09
U 1 1 5B6641D4
P 2100 5350
F 0 "#PWR09" H 2350 5100 50  0001 C CNN
F 1 "Earth_Protective" H 2550 5200 50  0001 C CNN
F 2 "" H 2100 5250 50  0001 C CNN
F 3 "" H 2100 5250 50  0001 C CNN
	1    2100 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 4900 2100 4900
Wire Wire Line
	2100 4900 2100 5350
Text GLabel 2450 4800 2    60   Input ~ 0
Vbat
Wire Wire Line
	1950 4800 2950 4800
Wire Wire Line
	6950 2650 5150 2650
Wire Wire Line
	5150 2650 5150 5450
Wire Wire Line
	5150 5450 2950 5450
Wire Wire Line
	2950 5450 2950 4800
$EndSCHEMATC
